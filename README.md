# sandstone_image

### 运行截图



### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

### 软件架构
软件架构说明



### 功能
本项目实现了以下功能：

读取砂岩截面的灰度图像及其对应的分区图
将图像数据转换为适合机器学习模型的格式
使用随机森林算法训练图像分区模型
计算并展示模型在测试数据上的准确率
使用训练好的模型对新的砂岩截面图像进行分区预测


### 依赖

- 本项目依赖以下库：
- 
- sklearn
- gradio
- pickle
- numpy
- cv2



### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

### 个人信息

- 学号: 202152320219
- 年级: 2021
- 专业: 智能科学与技术
- 班级: 2班

